function SortingHelper(arr, compare) {


    this.cacheVals = [] // keep cache of items, might be expensive to calculate value for each comparisson
    this.indexes = [] // array of indices, we actually sort this array instead of arr

    this.update(arr) // change values

    this.customCompare = compare

    return this;
}

SortingHelper.prototype.data = function(){
	return this.arr;
}

SortingHelper.prototype.update = function(arr){
	this.arr = arr		
	this.cacheVals.length = this.arr.length
}

SortingHelper.prototype.getValues = function(from, to){
	return this.indexes.slice(from, to).map((idx) => this.arr[idx])
}

SortingHelper.prototype._compare = function(idx1, idx2) {
	this.cnt++
	//idx1 == idx2 && console.log(idx1, idx2)
	if(this.compareToUse(this.cacheVals[idx1],  this.cacheVals[idx2])) return 1;
	if(this.compareToUse(this.cacheVals[idx2],  this.cacheVals[idx1])) return -1;

	return idx1 < idx2;

}

SortingHelper.prototype.sort = function(from, to){
	this.quickSort(from, to);
}

SortingHelper.prototype.quickSort = function(from, to) {
	this.cnt = this.swaps = 0

	this.from = from == undefined ? 0 : from;
	this.to = to == undefined ? this.arr.length - 1 : to;

	this.compareToUse =  this.customCompare ? this.customCompare : (a, b) =>  a < b;

	var getValue = this.customValue ? this.customValue : (a) => a

	var oldLength = this.indexes.length

	this.cacheVals.length = this.indexes.length = this.arr.length

	for(var i = 0; i < this.arr.length; i++){
		this.cacheVals[i] = (getValue(this.arr[i], i)) 
	}

	for(var i = oldLength; i < this.arr.length; i++){
		this.indexes[i] = i;
	}

    this._quickSort(0, this.arr.length - 1, Math.max(Math.ceil(Math.log2(this.arr.length)) * 2, 5))

	console.log("comparissons", this.cnt, "swaps", this.swaps)

}

SortingHelper.prototype.insertionSort = function(left, right){ // interval [)
	for(var i = left + 1; i < right; i++){
		var tmp = this.indexes[i];
		var j = i;
		while(--j >= left && this._compare(tmp, this.indexes[j]) == 1){
			this.indexes[j + 1] = this.indexes[j]
			this.swaps++;
		}
		this.indexes[j + 1] = tmp;
		this.swaps++
	}
}

SortingHelper.prototype._quickSort = function(left, right, limit) { // interval []
    if (left >= right) return;

    if(right - left <= 10){
    	this.insertionSort(left, right + 1)
    	return;
    }

    if(limit == 0){
    	this.heapsort(left, right + 1);
    	return;
    }

    var pivot = this.indexes[left + Math.floor((right - left) / 2)];

    var i = left, j = right

     while (i <= j){
        while (this._compare(this.indexes[i], pivot) == 1) {
            i++;
        }
        while (this._compare(pivot, this.indexes[j]) == 1) {
            j--;
        }
        if (i <= j) {
        	var temp = this.indexes[i]
		    this.indexes[i++] = this.indexes[j]
		    this.indexes[j--] = temp
		    this.swaps++
        }
    }

    if(true || this.from < j && this.to >= left){
    	this._quickSort(left, j, limit - 1);	
    }

    if(true || this.to >= i && this.from < right){
    	this._quickSort(i, right, limit - 1);
	}
}


SortingHelper.prototype.heapsort = function(left, right) { // interval [)
    var n = right - left,
        i = Math.floor(n / 2),
        parent, child, t
    while (true) {
        if (i > 0) {
            t = this.indexes[--i + left]
        } else {
            if (--n <= 0) return;
            t = this.indexes[left + n]
            this.indexes[left + n] = this.indexes[left]
            this.swaps++
        }
        parent = i;
        child = i * 2 + 1;
        while (child < n) {
            if (child + 1 < n && this._compare(this.indexes[child + 1 + left], this.indexes[child + left]) == 1) {
                child++;
            }
            if (this._compare(this.indexes[child + left], t) == 1) {
                this.indexes[parent + left] = this.indexes[child + left];
                this.swaps++
                parent = child;
                child = parent * 2 + 1;
            } else {
                break;
            }
        }
        this.indexes[parent + left] = t;
        this.swaps++
    }
}