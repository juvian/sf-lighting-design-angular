var PaginationMixin = (Base) => class extends Base {
	initializePagination () {
		this._total = 0;
		this.pageSize = 1;
		this._currentPage = 0;
		this.pagesToShow = 5;	
	}

	get total () {
		return this._total;
	}

	get pages () {
		return Math.ceil(this.total / this.pageSize); 
	}

	get page () {
		return this._currentPage + 1;
	}

	get from () {
		return Math.min(this._currentPage * this.pageSize + 1, this.total);
	}

	get to () {
		return Math.min(this.page * this.pageSize, this.total);
	}

	set page (page) {
		if (page < 1 || page > this.pages) throw new Error(`Page ${page} does not exist, must be in range 1 - ${this.pages}`);
		this._currentPage = page - 1;
	}

	set total (total) {
		this._total = total;
	}

};