class SldsPagination extends PaginationMixin(SldsButtonGroup) {

		constructor () {
			super();
			this.initializePagination();
			this.firstText = "First";
			this.lastText = "Last";
		}

		connectedCallback () {
			super.connectedCallback();
		}

		disconnectedCallback () {
			super.disconnectedCallback();
		}

		_removeListeners () {
			this.removeEventListener("button-selected", this._pageChange, {passive : true});
			super._removeListeners();
		}

		_addListeners () {
			this.addEventListener("button-selected", this._pageChange, {passive : true});
			super._addListeners();
		}

		set page (page) {
			super.page = page;
			this.dispatchEvent(new CustomEvent("page-change", {detail : page}));
		}

		get page () {
			return super.page;
		}

		_pageChange (e) {
			var page = e.detail;

			if (page && page != this.page) {
				this.page = page;
				this.render()
			}
		}

		_getPageButton (i) {
			var el = document.createElement('slds-button')
			el.value = i
			el.selected = i == this.page
			el.innerText = this._getText(i) 
			return el;
		}


		_getText (i) {
			if (i == 1) return this.firstText;
			if (i == this.pages + 1) return this.lastText
			return i;
		}


		render () {

			var start = Math.max(this.page - Math.floor(this.pagesToShow / 2), 1);						
			var end = Math.min(start  + this.pagesToShow, this.pages + 1);

			var fragment = document.createDocumentFragment()

			if(end == this.pages + 1) {
				start = Math.max(end - this.pagesToShow, 1);
			} else {
				end--;
			}

			if (start != 1) {
				fragment.appendChild(this._getPageButton(1))
				start ++;
			}

			for(var i = start; i < end; i++) {
				fragment.appendChild(this._getPageButton(i));
			}

			if (end != this.pages + 1) {
				fragment.appendChild(this._getPageButton(this.pages + 1));						
			}

			this.buttonsContainer.innerHTML = ""
			this.buttonsContainer.appendChild(fragment)

		}

	}

customElements.define("slds-pagination", SldsPagination);