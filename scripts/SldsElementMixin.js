var SldsElementMixin = (Base) => class extends Base {

	constructor () {
		super();
		this._initialized = false;
	}

	attributeChangedCallback(attr, oldValue, newValue) {
    	if (attr in this && !(attr in HTMLElement.prototype)) {
        	this[attr] = newValue;
      	}
  	}

  	static get observedAttributes() {
  		console.log(this.observedAttributes())
  		return this.observedAttributes() || [];
  	}

  	connectedCallback () {
  		if (this._initialized == false) {
  			this._initialized = true;
  			this.initialize && this.initialize();
  		} else {
  			this.connect && this.connect();
  		}
  	}	
  
}