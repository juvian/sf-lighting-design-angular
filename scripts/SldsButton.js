class SldsButton extends HTMLElement {

  	static get observedAttributes() {
  		return ["type", "disabled", "value"];
  	}

	constructor () {
		super();
		this._initialized = false;
		this._listenersAdded = false;
		this.element = document.createElement("button");
		this.element.className = "slds-button";
	}

	connectedCallback () {

		if (this._initialized == false) {
			this._initialized = true;
			this.style.display = "inline-block";

			this.type = this.type
			this.element.disabled = this.hasAttribute("disabled") && this.getAttribute("disabled") == "false";
			this.element.value = this.getAttribute("value")

			SldsUtils.appendContent(this.element, this);
			this.dispatchEvent(new CustomEvent("btn-initialized", {detail : this.element, bubbles : true}));
		}

		if (this._listenersAdded == false) {
			this._addListeners();
		}

	}

	disconnectedCallback () {
		if (this._listenersAdded) {
			this._removeListeners();
		}
	}

	attributeChangedCallback (attr, oldValue, newValue) {
		this[attr] = newValue;
	}

	_addListeners () {
		this._listenersAdded = true;
		this.observer = SldsUtils.replaceNewNodes(this);
	}

	_removeListeners () {
		this._listenersAdded = false;
		this.observer.disconnect();
	}

	get type () {
		return this.getAttribute("type") || "neutral";
	}

	set type (val) {
		this.setAttribute("type", val)
		SldsUtils.replaceClasses(this.element, [new RegExp("slds-button--.*")], "slds-button--" + this.type, true)
	}

	get disabled () {
		return this.element.disabled;
	}

	set disabled (val) {
		this.element.disabled = val;
	}

	get value () {
		return this.element.hasAttribute("value") ? this.element.getAttribute("value") : this.element.innerText
	}

	set value (val) {
		this.element.setAttribute("value", val)
	}


}

customElements.define("slds-button", SldsButton);