class SldsDatePicker extends HTMLElement {

	constructor () {
		super();
		this._rendered = false;
		this._months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		
		this._days = ["Sunday", "Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday"]

		this._currentMonth = new Date().getMonth()
		this._currentYear = new Date().getFullYear()

		this._years = new Array (31);

		for (var i = -15; i <= 15; i ++) {
			this._years[i + 15] = this._currentYear + i;
		}

		this._from = null;
		this._to = null;

	}

	connectedCallback () {
		this.innerHTML =  ` <div class="slds-datepicker slds-dropdown slds-dropdown--left" aria-hidden="false">
							  <div class="slds-datepicker__filter slds-grid">
							    <div class="slds-datepicker__filter--month slds-grid slds-grid--align-spread slds-grow">
							      <div class="slds-align-middle">
							        <button class="slds-button slds-button--icon-container" data-action = "previous">
							          <svg aria-hidden="true" class="slds-button__icon">
							            <use xlink:href="/assets/icons/utility-sprite/svg/symbols.svg#left"></use>
							          </svg>
							        </button>
							      </div>
							      <h2 class="slds-align-middle month" aria-live="assertive" aria-atomic="true"></h2>
							      <div class="slds-align-middle">
							        <button class="slds-button slds-button--icon-container" data-action = "next">
							          <svg aria-hidden="true" class="slds-button__icon">
							            <use xlink:href="/assets/icons/utility-sprite/svg/symbols.svg#right"></use>
							          </svg>
							        </button>
							      </div>
							    </div>  
							    <div class="slds-shrink-none">
								  <div class="slds-select_container">
								    <select class="slds-select year"></select>
								  </div>
								</div>
						      </div>
							  <table class="slds-datepicker__month" role="grid" aria-labelledby="month">
								<thead>
									  <tr class = "weekdays"></tr>
									</thead>	
									<tbody class = "days"></tbody>
								  </table>
								</div> `				
		
		this._rendered = true;
		
		this._month = this.getElementsByClassName("month")[0]
		this._year = this.getElementsByClassName("year")[0]
		this._day = this.getElementsByClassName("days")[0]

		this.addEventListener("click", this.handleClick.bind(this), {passive : true})

		this.render();

	}

	disconnectedCallback () {
		this.removeEventListener("click", this.handleClick.bind(this), {passive : true})
	}	

	get isRange () {
		return this.getAttribute("range") == "true";
	}

	handleClick (e) {
		var el = e.target.closest("button")

		if (el != null) {
			var action = el.getAttribute("data-action")

			if (action == "next") {
				this.nextMonth()
			} else {
				this.previousMonth()
			}

			this.dispatchEvent(new CustomEvent('month-changed', { detail : action}));
			
			this.render()

		} else {
			el = e.target.closest("td")
				
			if (el != null) {

				if (el.getAttribute("aria-disabled") == "false") {

					var day = new Date (el.getAttribute("data-year"), el.getAttribute("data-month"), el.getAttribute("data-day"));

					if (el.classList.contains("slds-is-selected")) {

						if (this.equalDates(this._from, day)) {
							this._from = null;
							this.dispatchEvent(new CustomEvent('from-deselected'));
						} else if (this.equalDates(this._to, day)) {
							this._to = null;
							this.dispatchEvent(new CustomEvent('to-deselected'));
						} else {
							this._to = day;
							this.dispatchEvent(new CustomEvent('range-changed'));
						}

					} else {
						if (this.isRange) {
							if (this._from == null) {
								this._from = day;
								this.dispatchEvent(new CustomEvent('from-selected'));
							} else if (this._to == null) {
								this._to = day;
								this.dispatchEvent(new CustomEvent('to-selected'));
							} else if (day < this._from) {
								this._from = day;
							} else {
								this._to = day;
							}

							if (this._from > this._to) {
								var temp = this._from
								this._from = this._to
								this._to = temp
							}

							this.dispatchEvent(new CustomEvent('range-changed'))

						} else {
							this._from = day;
							this.dispatchEvent(new CustomEvent('from-changed'));
						}
					}

					this.renderDays()

				}

			} else {
				el = e.target.closest("select")

				if (el != null && el.value != this._currentYear) {
					this._currentYear = el.value;
					this.dispatchEvent(new CustomEvent('year-changed'));


					this.renderDays()
				}
			}

		}

	} 

	renderMonth () {
		if (this._rendered == false) return;

		this._month.innerHTML = this.getMonth(this._currentMonth);	
	}

	renderYears () {
		if (this._rendered == false) return;

		var html = "";

		for (var i = 0; i < this._years.length; i++) {
			var year = this._years[i];
			html += `<option value = '${year}' ${year == this._currentYear ? "selected" : ""}>${this.getYear(year)}</option>`
		}

		this._year.innerHTML = html;

	}

	renderDays () {
		if (this._rendered == false) return;

		var firstDay = new Date(this._currentYear, this._currentMonth, 1);
		var lastDay = new Date(this._currentYear, this._currentMonth + 1, 0);				
	
		firstDay.setDate(firstDay.getDate() - firstDay.getDay())
		lastDay.setDate(lastDay.getDate() + 6 - lastDay.getDay())

		var html = this.getDays(); 

		while (firstDay <= lastDay) {

			if (firstDay.getDay() == 0) {
				html += "<tr class = 'slds-has-multi-row-selection'>"
			}

			var cls = this.getClass(firstDay)

			html += `<td data-year = "${firstDay.getFullYear()}" data-month = "${firstDay.getMonth()}"  data-day = "${firstDay.getDate()}" class = "${cls}" role="gridcell" aria-disabled = "${this.isDisabled(firstDay)}" aria-selected="${cls.includes("selected")}">
  					 	<span class="slds-day">${firstDay.getDate()}</span>
  					 </td>`
				
				if (firstDay.getDay() == 6) {
				html += "</tr>"
			}       	

			firstDay.setDate(firstDay.getDate() + 1);

		}

		this._day.innerHTML = html;

	}


	render () {
		this.renderMonth();
		this.renderYears();
		this.renderDays();
	}

	nextMonth () {
		this._currentMonth++;
		if (this._currentMonth == 12) {
			this._currentMonth = 0;
			this.nextYear(); 
		}
	}

	previousMonth () {
		this._currentMonth--;
		if (this._currentMonth == -1) {
			this._currentMonth = 11;
			this.previousYear();
		}
	}

	nextYear () {
		this._currentYear++;
	}

	previousYear () {
		this._currentYear--;
	}

	getMonth (month) {
		return this._months[month];
	}

	getYear (year) {
		return year;
	}

	getDay (day) {
		return day.substring(0, 3);
	}

	getFullDay (day) {
		return day;
	}

	getDays () {
		
		var html = "<tr class = 'weekdays'>"

		for (var i = 0; i < this._days.length; i++) {
			html += `<th scope="col">
			          <abbr title="${this.getFullDay(this._days[i])}">${this.getDay(this._days[i])}</abbr>
			        </th>`
		}

		html+= "</tr>"

		return html
	}

	getClass (day) {
		
		if (day.getMonth() != this._currentMonth) {
			return "slds-disabled-text"
		} else if (this._from && this._to && this._from <= day && this._to >= day) {
			return "slds-is-selected slds-is-selected-multi"
		} else if ( this.equalDates(this._from, day)  || this.equalDates(this._to, day)) {
			return "slds-is-selected"
		}

		return ""
	}

	isDisabled (day) {
		return day.getMonth() != this._currentMonth
	}

	equalDates (day1, day2) {
		return day1 && day2 && day1.toDateString() == day2.toDateString();
	} 


}

customElements.define("slds-date-picker", SldsDatePicker);