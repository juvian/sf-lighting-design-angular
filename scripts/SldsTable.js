class SldsTable extends PaginationMixin(HTMLElement){

	constructor(){
		super();
		this.initializePagination();
		this._columns = [];
		this._rendered = false;
		this._sortBy = {};
		this._data = [];
		this._rowsRendered = []
		this._reverse = false;
		this._sortable = true;
		this._sortByField = null;
		this._dirtyCheck = true;
		this._debug = false;

		this._cache = {
			enabled : true,
			dataIndexes : {},
			dataCache : {},
			dataIndexesDirty : []
		}

	}

	get total () {
		return this._data.length;
	}

	get dirtyCheck () {
		return this._dirtyCheck;
	}

	get columns(){
		return this._columns
	}

	get reverse () {
		return this._reverse
	}

	get data () {
		return this._data
	}

	get sortBy () {
		return this._sortBy.field;
	}

	get dataIndexes () {
		if(this.sortBy == null) throw new Error("A sort By column must be set")
		return this._cache.dataIndexes[this.sortBy];
	}

	get dataCache () {
		if(this.sortBy == null) throw new Error("A sort By column must be set")
		return this._cache.dataCache[this.sortBy];
	}

	get rowsRendered () {
		return this._rowsRendered
	}

	set data(data){
		this._data = data;
	}

	set columns(columns){
		this._columns = columns
		this.sortBy = this._sortByField;
	}

	set reverse(rev){
		this._reverse = rev;
	}

	set sortBy(field){
		this._sortByField = field;

		for (var i = 0; i < this._columns.length; i++){
			if (this._columns[i].field == field){

				if (this._cache.enabled == false && this.sortBy != null) {
					delete this._cache.dataIndexes[this.sortBy];
					delete this._cache.dataCache[this.sortBy];
				}

				this._sortBy = this._columns[i]

				this._cache.dataIndexes[this.sortBy] = [];
				this._cache.dataCache[this.sortBy] = [];

				break;
			}
		}

	}

	set sortable (val) {
		this._sortable = val;
	}

	set cache (val) {
		this._cache = !!val;
	}

	set dirtyCheck (val) {
		this._dirtyCheck = !!val;
	}

	_defaultCustomValue (val) {
		return val[this._sortBy.field]
	}

	_columnClickedListener (e) {
		var el = e.target.closest("th")
		if (this._sortable && el.tagName == 'TH'){
			var field = el.getAttribute("data-field")
			
			if (field == this.sortBy) {
				this.reverse = !this.reverse;
				this.renderRows([]);
			} else {
				this._reverse = false;
				this.sortBy = field;
				this.renderRows();
			}
		}
	}



	renderedIndex (idx) {
		if(idx < 1 || idx > this.dataIndexes.length) throw new Error("Invalid index, should be between 1 and " + this.dataIndexes.length)
		return this.dataIndexes[idx - 1];
	}

	columnsToRender () {
		return this._columns.filter(function(col){return col.hidden != true})
	}

	_time (str) {
		if(this._debug) {
			console.time(str)
		}
	}

	_timeEnd (str) {
		if(this._debug) {
			console.timeEnd(str)
		}
	}

	rowsToRender(dirtyIndexes){
		
		this._time("sort")

		var dataIndexes = this.dataIndexes
		var dataCache = this.dataCache

		var oldLength = dataIndexes.length

		dataCache.length = dataIndexes.length = this._rowsRendered.length = this._data.length 

		var getValue = this._sortBy.customValue ? this._sortBy.customValue : this._defaultCustomValue.bind(this)

		var dirty = dirtyIndexes || []

		this._time("dirtyCheck");

		for (var i = 0; i < dirty.length; i++) {
			dataCache[dirty[i]] = getValue(this._data[dirty[i]], dirty[i]);
		}

		for(var i = ((dirtyIndexes || !this._dirtyCheck) ? oldLength : 0); i < this._data.length; i++){
			var tmp = getValue(this._data[i], i);

			if(tmp !== dataCache[i]){
				dataCache[i] = tmp;
				dirty.push(i);
			}
		}

		this._timeEnd("dirty check")

		for(var i = oldLength; i < this._data.length; i++){
			dataIndexes[i] = i;
		}

		if(dirty.length){
			this._cache.dataIndexesDirty.length = this._data.length;
			var compareToUse = this._sortBy.customCompare ? this._sortBy.customCompare : (a, b) =>  a < b;
			

			var compareFunc = (idx1, idx2) => {
				if(compareToUse(dataCache[idx1],  dataCache[idx2])) return -1;
				if(compareToUse(dataCache[idx2],  dataCache[idx1])) return 1;

				return idx1 < idx2;
			}

			dirty.sort(compareFunc);

			var j = 0, i = 0, k = 0;

			while (k < dataIndexes.length) {
				
				if (i == dirty.length || j == oldLength) {
					
					while (k < dataIndexes.length)  this._cache.dataIndexesDirty[k++] = (i == dirty.length ? dataIndexes[j++] : dirty[i++] );

				}else {
					while(compareFunc(dataIndexes[j], dirty[i]) == -1){
						 this._cache.dataIndexesDirty[k++] = dataIndexes[j++];
					}

					if(dataIndexes[j] == dirty[i]){
						j++;
					}else{
						 this._cache.dataIndexesDirty[k++] = dirty[i++];
					}

				}
			}

			var tmp = dataIndexes
			dataIndexes = this._cache.dataIndexesDirty;
			this._cache.dataIndexesDirty = tmp;
		}

		this._cache.dataIndexes[this.sortBy] = dataIndexes

		this._timeEnd("sort")

		this._updateRenderedRows();

		return this._rowsRendered;
	}

	_updateRenderedRows () {
		var dataIndexes = this.dataIndexes

		var to = this.to - 1, from = this.from - 1;

		var length = this._rowsRendered.length = to - from + 1;

		for(var i = 0; i < length; i++){
			this._rowsRendered[i] = this._data[dataIndexes[this._reverse ?  this.total - from - i - 1 : i + from]];
		}
	}

	connectedCallback(){
		this.innerHTML = this._getTableHtml();	

		this._head = this.getElementsByTagName('thead')[0];
		this._body = this.getElementsByTagName('tbody')[0];
		this._rendered = true;
	}

	disconnectedCallback(){
		if (this._addedListener) {
			this._head.removeEventListener(this._columnClickedListener.bind(this), {passive: true})
			this._addedListener = false;
		}
	}

	_render(){
		this._renderColumns()
		this._renderRows()
	}

	_renderColumns(){
		if(!this._rendered) return;
		
		this._head.innerHTML = this._getColumnsHtml();

		this.dispatchEvent(new CustomEvent("columns-rendered"));

	}

	_getTableHtml () {
		return	`<table class="slds-table slds-table--bordered slds-table--fixed-layout" role="grid">
					<thead></thead>
					<tbody></tbody>
				</table>`		
	}

	_getColumnsHtml () {
		var html = '<tr class="slds-text-title--caps" scope = "col">';
		
		var cols = this.columnsToRender()

		for(var i = 0; i < cols.length; i++){
			var col = cols[i]

			var cls = col.field == this.sortBy ? (this._reverse ? "slds-is-sorted slds-is-sorted--desc" : "slds-is-sorted slds-is-sorted--asc") : ""

			html += this._getColumnHtml(col, cls);
		}

		html += '</tr>';

		return html;
	}

	_getColumnHtml (col, cls) {
		return `<th data-field = "${col.field}" class = "slds-is-sortable ${cls}" scope = "col" aria-label="${col.label}">
						<a href="javascript:void(0);" class="slds-th__action slds-text-link--reset">
				        	<span class = "slds-truncate" title = "${col.label}">${col.label}</span>
				        	<div class="slds-icon_container">
				            	<svg aria-hidden="true" class="slds-icon slds-icon--x-small slds-icon-text-default slds-is-sortable__icon">
				              		<use xlink:href="assets/icons/utility-sprite/svg/symbols.svg#arrowdown"></use>
				            	</svg>
				          	</div>
				        </a>
			      	</th>`
	}

	_renderRows(dirty) {
		if(!this._rendered) return;	

		if (!this._addedListener) {
			this._head.addEventListener("click", this._columnClickedListener.bind(this),  {passive: true});
			this._addedListener = true;
		} 

		this._body.innerHTML = this._getRowsHtml(dirty);

		this.dispatchEvent(new CustomEvent("rows-rendered"));

	}

	_getRowsHtml (dirty) {

		var html = '';

		var cols = this.columnsToRender()
		var rows = this.rowsToRender(dirty)

		for(var i = 0; i < rows.length; i++){
			html += '<tr>'
			var row = rows[i]
			for(var j = 0; j < cols.length; j++){
				var col = cols[j]
				var val = col.customDisplay ? col.customDisplay(row[col.field]) : row[col.field]
				
				if(val == undefined || val == null){
					val = ""
				}

				html +=	this._getRowHtml(row, col, val);	        
			}

			html += '</tr>'
		}

		return html;
	}

	_getRowHtml (row, col, val) {
		return `<th scope="gridcell" data-label="${col.label}">
       		<div class="slds-truncate" title="${row[col.field]}">${val}</div>
        </th>`
	}

	renderRows (dirty) {
		this._renderRows(dirty);
	}

	render () {
		this._render();
	}

} 

customElements.define("slds-table", SldsTable);



/*
window.onload = function(){
				var table = window.table = document.getElementsByTagName('slds-table')[0]
				var data = []
				for(var i = 0; i < 100000; i++){
					data.push({name : Math.floor(Math.random() * 1000), age: i % 10})
				}

				table.dirtyCheck = false;

				table.sortBy = "age"
				table.columns = ([{label:"Name", field: 'name'}, {label:"a"},{label:"b"}, {label: "Age", field: 'age', customDisplay : (a) => '<bold style="color:red">' + a + '</bold>'}])
				
				table.data = data
				
				data[123] = {name: 4, age: -1 }

				table.updateRows([123])

				table.columns[1].hidden = true;

				table.update()

			}

*/