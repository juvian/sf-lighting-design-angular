class SldsButtonGroup extends HTMLElement {

  	static get observedAttributes() {
  		return ["multi-select"];
  	}

	constructor () {
		super();
		this._listenerAdded = false;
		this._initialized = false;
		this.element = document.createElement("div");
		this.element.className = "slds-button-group";
		console.log(this.innerHTML)
	}

	connectedCallback () {
			console.log(this.innerHTML)
		
		if (this._initialized == false) {
			this._initialized = true;
			this.element.setAttribute("role", "group");
			this.style.display = "inline-block";

			this.multiSelect = this.hasAttribute("multi-select") && this.getAttribute("multi-select") != "false";
			SldsUtils.appendContent(this.element, this);
		}

		if (this._listenerAdded == false) {
			this._addListeners();
		}

	}	

	disconnectedCallback () {
		if(this._listenerAdded) {
			this._removeListeners();
		}
	}

	attributeChangedCallback (attr, oldValue, newValue) {
		SldsUtils.attributeChange(this, attrs, newValue);
	}

	_removeListeners () {
		this.element.removeEventListener("click", this._clickHandler.bind(this), {passive : true});
		this.removeEventListener("initialized", this._childrenHandler.bind(this));
		this._listenerAdded = false;
	}

	_addListeners () {
		this.element.addEventListener("click", this._clickHandler.bind(this), {passive : true});
		this.addEventListener("btn-initialized", this._childrenHandler.bind(this));
		this._listenerAdded = true;
	}

	_childrenHandler (e) {
		console.log(e.detail)
		//this.element.appendChild(e.detail)
	}

	_clickHandler (e) {
		var el = e.target.closest("slds-button");
		var value = el.getAttribute("value");

		console.log(el, value)

		var selected = el.type == "brand"

		if (this.multiSelect == false) {
			var old = this.getElementsByClassName("slds-button--brand");

			if (old.length) {
				old[0].type = "neutral";
			}

			el.type = "brand";
		} else {
			el.type = selected ? "neutral" : "brand";
		}

		this.dispatchEvent(new CustomEvent("button-click", {detail : value}));

		this.dispatchEvent(new CustomEvent("button-" + selected ? "selected" : "deselected", {detail : el}))

	}


	get multiSelect () {
		return this.element.hasAttribute("multi-select") && this.element.getAttribute("multi-select") != "false";		
	}

	set multiSelect (val) {
		this.element.setAttribute("multi-select", !!val)
	}

}

customElements.define("slds-button-group", SldsButtonGroup);


