class SldsUtils {

	static matchClasses (el, classes) {

		var regex = []
		var strings = {}

		for (var i = 0; i < classes.length; i++) {
			if (classes[i] instanceof RegExp) {
				regex.push(classes[i])
			} else {
				strings[classes[i]] = true
			}
		}

		var matches = []

		for (var i = 0; i < el.classList.length; i++) {
			if (strings.hasOwnProperty(el.classList[i])) {
				matches.push(el.classList[i]);
			} else {
				for (var j = 0; j < regex.length; j ++) {
					if (el.classList[i].match(regex[j])) {
						matches.push(el.classList[i]);
						break;
					}
				}
			}
		}		

		return matches;
	}

	static removeClasses (el, classes) {
		var matches = SldsUtils.matchClasses(el, classes);
		
		for (var i = 0; i < matches.length; i++) {
			el.classList.remove(matches[i]);
		}
		
		return matches;
	}

	static replaceClasses (el, classes, replacement, force) {
		var matches = SldsUtils.removeClasses(el, classes);
		if (matches.length || force) el.classList.add(replacement);
	}

	static appendContent (el, contentNode) {
		var childs = contentNode.childNodes;

		for (var i = 0; i < childs.length; i++) {
			el.appendChild(childs[i]);
		}

		contentNode.appendChild(el);		
	}

	static attributeChange (customElement, attribute, value) {
		var propName = attribute.split("-").map((v, idx) => (idx > 0 ? v.charAt(0).toUpperCase() : v.charAt(0)) + v.slice(1).toLowerCase()).join("");
		customElement[propName] = value;
	}

	static observeNewNodes (el, callback) {
		var observer = new MutationObserver(function(mutations) {
			for(var i = 0; i < mutations.length; i++) {
				for(var j = 0; j < mutations[i].addedNodes.length; j++) {
					callback(mutations[i].addedNodes[j]);
				}
			}    
		});

		observer.observe(el, {childList : true});

		return observer;

	}

	static appendNode (customElement, element) {
		customElement.element.appendChild(element);
	}

	static replaceNewNodes (customElement) {
		return SldsUtils.observeNewNodes(customElement, SldsUtils.appendNode.bind(customElement));
	}

}